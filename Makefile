CC=gcc
OUT=calculator
CCFLAGS=-W -g

.PHONY: all objs clean
all: objs
	$(CC) src/main.c $(wildcard src/objs/*.o) -o $(OUT) -I src/headers/ $(CCFLAGS)
objs:
	+$(MAKE) -C src/
clean:
	rm src/objs/*.o
