#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>

#define BUFFER_MAX 255
char ignoredChars[] = " \t\n\r\v\a";

// User input parsing functions
void remove_whitespaces(char* input, char* output);

int main() {
	char buffer[BUFFER_MAX];
	printf("# El maximo de caracteres son %d caracteres\n", BUFFER_MAX);
	printf("Escribe '?' para ver la ayuda\n");
	printf("Ingresa la operación a calcular\n\n");
	while(1) {
		char parsed_input[BUFFER_MAX];
		printf("]: ");
		fgets(buffer, BUFFER_MAX, stdin);
		remove_whitespaces(buffer, parsed_input);
		// Check if the function works
		printf("%s\n", parsed_input);
		break;
	}
	return 0;
}

void remove_whitespaces(char* input, char* output) {
	int pos = 0;
	ssize_t inputLenght = strlen(input);
	ssize_t ignoredLenght = strlen(ignoredChars);
	if(inputLenght > BUFFER_MAX) {	
		fprintf(stderr, "[Error] El input es mas grande que el permitido\n");
		exit(EXIT_FAILURE);
	}

	for(unsigned int i = 0; i < inputLenght; i++) {
		int is_ignored = 0;
		for(unsigned int j = 0; j < ignoredLenght; j++) {
			if(input[i] == ignoredChars[j]) {
				is_ignored = 1;
				break;
			}
		}

		if(is_ignored) continue;
		output[pos] = input[i];
		pos++;
	}
}
